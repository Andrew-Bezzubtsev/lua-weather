local io = require("io") 
local json = require("json")
local tonumber = tonumber

local function scriptPath()
	local str = debug.getinfo(2, "S").source:sub(2)
	return str:match("(.*/)")
end

local function getContent(p)
	local f = io.open(p, "r")
	local data = f:read("*a")
	f:close()

	return data
end

local key = getContent(scriptPath() .. "key.txt"):gsub("%s+", "")

local function cmd(c)
	local f = io.popen(c)
	local data = f:read("*a")
	f:close()

	return data
end

local function curl(url)
	return cmd("curl --connection-timout 3 " .. url .. " 2> /dev/null")
end

local function getLocation()
	local country = curl("ipinfo.io/country"):gsub("%s+", "")
	local city = curl("ipinfo.io/region"):gsub("%s+", "")

	return country .. "/" .. city
end

function weatherInfo()
	local url = "api.wunderground.com/api/" .. key .. "/conditions/q/" .. getLocation() .. ".json"
	local info = json.decode(curl(url))
	
	if not info or not info.current_observation then
		return nil
	end

	local function get(name)
		return info["current_observation"][name]
	end

	if info.error then
		error(info.error.description)
	end

	local this = {
		tempc    = get("temp_c"),
		tempf    = get("temp_f"),
		wind_dir = get("wind_dir"),
		wind_deg = get("wind_degrees"),
		wind_mph = get("wind_mph"),
		wind_kph = get("wind_kph"),
		humidity = get("relative_humidity"),
		weather  = get("weather"),
		pressure = get("pressure_mb"),
		feelc    = get("feelslike_c"),
		feelf    = get("feelslike_f"),
		icon     = get("icon")
	}

	return this
end
